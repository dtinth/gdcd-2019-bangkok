global diversity CFP day (Bangkok) 2019
=======================================

Event Agenda
------------

This is a rough agenda and is subject to change.

| Time | Activity |
| ---- | -------- |
| 13:00 | Arrival and welcome |
| 13:30 | What should I speak about?<br/>*With CFP advice from Raquel Velez and Sarah Mei ([video](https://www.youtube.com/watch?v=lHIHgauh000))* |
| 14:10 | Proposal writing<br />*With feedback from mentors* |
| 14:30 | Crafting your bio<br />*With advice from Danielle Barnes from @WomenTalkDesign ([video](https://www.youtube.com/watch?v=GPPnvXlVj7w))* |
| 15:00 | Break |
| 15:15 | Caring for your audience |
| 15:45 | Your perfect tech talk<br />*With a presentation by Saron Yitbarek ([video](https://www.youtube.com/watch?v=AzVr_nsKoZs))* |
| 16:05 | The art of slide design<br />*With a presentation by Melinda Seckington ([video](https://www.youtube.com/watch?v=e5gwEvQah-s))* |
| 16:30 | Proposal writing<br />*With feedback from mentors* |
| 17:00 | The day of your talk<br />*With advice from Jessica Rose ([video](https://www.youtube.com/watch?v=m2j6-pjfvuo))* |
| 17:30 | Attendees present their talk proposal |
| 17:45 | Family Photo and Close |

Code of Conduct
---------------
**We value the diversity and inclusion of underrepresented and marginalized people in tech.**

This event follows the [global diversity CFP day Code of Conduct](https://www.globaldiversitycfpday.com/code-of-conduct), and it will be enforced throughout the event.

If someone behaves inappropriately or makes you feel uncomfortable, then that’s something that must be addressed. Please contact our team member at the workshop who will help mediate and resolve the issue as appropriate.

If the person you need to report is a member of the workshop team please contact the workshop organiser, and if the person you need to report is the workshop organiser, please [contact the Global Code of Conduct Team](mailto:coc@globaldiversitycfpday.com?Subject=Code%20of%20Conduct%20Issue&body=Location%0A%0AIssue).

If you don’t feel comfortable reporting an issue in person, you can [report via our online form](https://goo.gl/forms/h4cLFtdjwebgSGIx2).

Ideation questions: What should I speak about?
------------------------------------
- What are you interested in?
- What are you excited about?
- What are you really annoyed about?
- What have you learnt in the past 3 months, 1 year?
- What is the most recent thing you learnt that made you feel wow?
- What are you curious to learn more in the next 3 months, 1 year, 5 years?
- What you wanted to do/learn but havn't got the time to do so? 
- What is the most fun thing you've done recently? 
- What is the most embarrassing thing you've done recently?
- What is the proudest achievement you’ve made in the past 3 months, 1 year, 5 years?
- What do you regret having spent time on?
- What is unique about you?

Dig deeper:

- 5 Whys: Ask “why?” 5 times to dig deeper into their interest.

Ideation questions: What should I include in my bio?
--------------------------------
- What are you studying/working on?
- What is your proudest achievement?
- What people would say when they’re asked to describe you?
- What are 3 things that people rarely know about you?

Feedback
--------
We hope you enjoyed the workshop. But we are also here to learn how we could have done it better. Therefore, your feedback would be much appreciated. Please [send us your feedback](https://airtable.com/shrPVIDFrz56oz2ie).
