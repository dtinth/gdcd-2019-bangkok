window.EVENT_INFO = {
  // Regenerate with the following code:
  /*
  copy($$('.volunteer').map(el => ({
    name: el.querySelector('.volunteer-name').textContent.trim(),
    reason: el.querySelectorAll('b')[0].nextSibling.nodeValue.trim(),
    about: el.querySelectorAll('b')[1].nextSibling.nodeValue.replace(/:/, '').trim(),
    image: el.querySelector('img').src
  })))
  */
  team: [
    {
      name: 'Thai Pangsakulyanont',
      reason:
        'Want to see more speakers from Thailand, both locally, and in an international stage.',
      about: 'JavaScript musician?????',
      image: 'https://github.com/dtinth.png'
    },
    {
      name: 'Pallop Chaoputhipuchong',
      reason: 'I wanna make this happen by organizing it in Bangkok, Thailand.',
      about:
        "My name is Ham. I'm a web developer, who passionate to bring joy to people. I'm actively involved in the Thailand tech community.",
      image:
        'https://pbs.twimg.com/profile_images/1036164210508460037/1Asr6hRV_400x400.jpg'
    },
    {
      name: 'Sudarat Chattanon',
      reason:
        "I like to share my experience about talking in public especially for the tech events and I also want to help people to be brave to propose their idea and confident. So let's have fun together!",
      about:
        'Happiness Software Engineer @ Pronto Tools. An organizer @ Girls Who Dev.',
      image:
        'https://scontent.fbkk8-2.fna.fbcdn.net/v/t1.0-9/50799531_10215498132140788_9019036313457786880_n.jpg?_nc_cat=107&_nc_eui2=AeEakFjS6iGpeD1LhvrBr_Xg1458W5JITul9r9JSC75TSPVrR6A2xuj1mQ1iaGFBwTzJbutfqebLO61UlYtuZ1ZzhhCum6msaIzLosuFcD23Vg&_nc_oc=AQloPjMgqXxClqC8uPeVcrfAm6v9nbNOsNRi-ZRe1cOypCUMUB-T3Z2pLm6kmDmnffM&_nc_ht=scontent.fbkk8-2.fna&oh=38dda5b21a2596023922a6640a825f80&oe=5CE9F2FC'
    },
    {
      name: 'Unnawut Leepaisalsuwanna',
      reason:
        'Public speaking is one of the best way to share knowledge as well as to receive feedbacks on your knowledge.',
      about:
        'Software Engineer at OmiseGO. TED Translator. TEDxBangkok Organizer 2015 - 2018.',
      image:
        'https://cdn-images-1.medium.com/max/1200/1*FLlg7tDr0jcjA6Dlqm1gLA.png'
    },
    {
      name: 'Antira Loachote',
      reason:
        "I'm interested to talk about tech to present with tech people and business people, I like communication about tech. I hope to see more speakers.",
      about: "I'm Pan. Frontend Adventurer @Prontotools",
      image:
        'https://scontent.fbkk8-2.fna.fbcdn.net/v/t1.0-9/52051020_2008627785856917_8159874474359914496_o.jpg?_nc_cat=107&_nc_eui2=AeE7G7cxzoMl8kerLYCXgcDYssV_RBdO8rwFqio-iOPGuKQe0jjfTdEP0toX1S-12khkJNFcrZp71sm9bN9BTplx85RKV4LAidA-G6wozvcuHw&_nc_ht=scontent.fbkk8-2.fna&oh=05138dc0cc6e688d2c7d11b84b7fe999&oe=5D223439'
    },
    {
      name: 'Phoomparin Mano',
      reason:
        "I'm interested in mentoring the attendees and helping them with the storytelling process and writing the proposals for tech conferences, especially if it's their first time writing a proposal.",
      about:
        "A 17 y/o software developer at OmniVirt. Passionate about building products and fostering communities to enhance people's lives with technology.",
      image: 'https://phoom.in.th/phoom.jpg'
    },
    {
      name: 'Yothin Muangsommuk',
      reason:
        'Just want to share my experience when submitting CFP and share experience on Python community about this',
      about:
        "Yothin is a Pythonista at Pronto Tools.He has been writing Python since 2010 with love on Pythonic way and the zen of Python. While he not doing Python stuff he's a Trekkie.",
      image:
        'https://secure.gravatar.com/avatar/0c501f2da7d6df295b7da22c8c4f2c11?s=500'
    },
    {
      name: 'Kan Ouivirach',
      reason:
        "I'd love to share my experience as a tech/conference speaker and hope it will be useful to the audience.",
      about:
        'Lead Software Architect @prontotools. Passionate in software engineering, data science, and data engineering.',
      image:
        'https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-9/50562846_2278174312214395_1641376354085634048_o.jpg?_nc_cat=107&_nc_eui2=AeHZjICUZFjjSn28W_d0nlO4GB16AK69WqMrSM5vRZMm_ei43hL3iAawa9f9Vo-kNIImHN6RhQxq7DXVCVEbgxpBj_TW10y1dn-zpPKuosLbyA&_nc_ht=scontent.fbkk5-7.fna&oh=ed4b91372c1316da2e363a635d683974&oe=5D27E08A'
    },
    {
      name: 'Iñaki Villar',
      reason:
        'Helping to the community to share my experience as speaker and developer',
      about:
        'Android Developer since 7 years ago. I worked with Bank apps in Spain for three years. Later I moved to Ireland to work with apps of Airlines and GSM carriers. Actually, I’m living in Thailand working for Agoda. I’m Google Developer Expert and I enjoy being involved with the Android community.',
      image:
        'https://avatars1.githubusercontent.com/u/475733?s=400&u=879d22b00633b174367313d5f5bce691b617d4a9&v=4'
    }
  ]
}
